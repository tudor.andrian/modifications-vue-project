// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
// import 'bootstrap'
import App from './App'
import router from './router'
import axios from 'axios'
import axiosCancel from 'axios-cancel'
import API from './api/API'
import Store from './api/Store'

Vue.use(BootstrapVue)

// axios.defaults.baseURL = 'http://localhost:8000'
axios.defaults.baseURL = 'http://api.pernador.cloud-us.eu'
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

axiosCancel(axios, {
  debug: true
})

window.axios = axios
window.API = API
window.Store = Store
window.router = router
window.moment = require('moment')

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  }
})
