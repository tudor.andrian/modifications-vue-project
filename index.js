import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'

// Views
import Dashboard from '@/views/Dashboard'
import Inventory from '@/views/Inventory'
import Materials from '@/views/inventory/Materials'
import Products from '@/views/inventory/Products'
import Sales from '@/views/inventory/Sales'
import GraphicMain from '@/views/graphs/GraphicMain'
import Test from '@/views/inventory/Test'

// Pages
import Login from '@/views/pages/Login'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: Full,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'inventory',
          name: 'Inventory',
          redirect: 'inventory/index',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'index',
              name: 'Index',
              component: Inventory
            },
            {
              path: 'materials',
              name: 'Materials',
              component: Materials
            },
            {
              path: 'products',
              name: 'Products',
              component: Products
            },
            {
              path: 'sales',
              name: 'Sales',
              component: Sales
            },
            {
              path: 'graphs',
              name: 'Grafice',
              component: GraphicMain
            },
            {
              path: 'test',
              name: 'Test',
              component: Test
            }
          ]
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
